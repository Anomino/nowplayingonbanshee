#!/usr/bin/env python3
# coding:UTF-8

"""
Bing APIを使って、画像を取得する
"""
import Setting
import urllib
import requests
import json
import io
import imghdr
import os

# 与えられた文字列を検索し。画像を取得
# 更に、与えられたパスに画像を保存
# 保存先のパスを返す
def GetImages(search_word, save_word, filepass) : 
    #print('検索文字列 : {0}'.format(search_word))

    params = {
            'Query' : "'{}'".format(search_word),
            'Market' : "'{}'".format('ja-JP'),
            '$format' : 'json',
            '$top' : '{0:d}'.format(Setting.SEARCH_LIMIT),
            '$skip' : '{0:d}'.format(0),
            }
    url = Setting.BING_URL + urllib.parse.urlencode(params)
    res_json = requests.get(url, auth = ('', Setting.API_KEY))
    res = json.loads(res_json.text)

    for result in res['d']['results'] : 
        image_url = result['MediaUrl']

        image_data = requests.get(image_url, timeout = 3)
        image_bin = image_data.content
        
        with io.BytesIO(image_bin) as fh : 
            image_type = imghdr.what(fh)

        if image_type == 'jpeg' : 
            extension = '.jpg'

        elif image_type == 'png' : 
            extension = '.png'

        else : 
            continue

        filename = save_word + extension

        if not os.path.isdir(filepass) : 
            os.mkdir(filepass)

        with open(os.path.join(filepass, filename), 'wb') as f : 
            f.write(image_bin)
            return os.path.abspath(filepass) + '/' + filename
        
if __name__ == "__main__" : 
    print(GetImages('アイドルマスターシンデレラガールズ', 'sumple',  'images'))
