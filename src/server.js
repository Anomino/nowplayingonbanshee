var express = require('express');
var app = express();
var http = require('http').Server(app);

app.use(express.static('public'));

http.listen(8800, function()
        {
            console.log('Server is listening at localhost : 8800');
        });

var io = require('socket.io')(http);

io.socket.on('connection', function(socket)
        {
            socket.on('play', function()
                    {
                        console.log('Play');
                    });

            socket.on('stop', function()
                    {
                        console.log('Stop');
                    });

            socket.on('next', function()
                    {
                        console.log('Next');
                    });

            socket.on('back', function()
                    {
                        console.log('Back');
                    });

        });
