#!/usr/bin/env python3
# coding:UTF-8

# NowPlayingOnBanshee

from requests_oauthlib import OAuth1Session
import Setting
import GetLyrics
import GetImages
import subprocess
import threading
import time
import json
import os
import sys
import re

# Bansheenのコマンド関連
BANSHEE       = "banshee"
CURRENT_STATE = "--query-current-state"
LAST_STATE    = "--query-last-state"
TITLE          = "--query-title"
ARTIST          = "--query-artist"
ALBUM          = "--query-album"

Twitter = OAuth1Session(Setting.CONSUMER_KEY, Setting.CONSUMER_SECRET, Setting.ACCESS_TOKEN, Setting.ACCESS_TOKEN_SECRET)

class NowPlayingOnBanshee(threading.Thread) : 
    def __init__(self, mode) : 
        threading.Thread.__init__(self)
        self.before = ''
        self.mode = mode

    def run(self) : 
        Info = GetBansheeInfo()
        
        if self.before != Info["title"] and Info['current'] == "playing": 
            showLyrics(Info)
            self.before = Info['title']

            if self.mode == '' : 

                res = TweetInfo(Info)
                if res == True : 
                    print("Tweeted successfull")

                else : 
                    print("Tweeted failure")

                print("----------------------------------")


        time.sleep(3)

def Main(mode = '') : 
    if mode == '' : 
        print('Start default mode')

    else : 
        print('Start off tweet mode')

    if not os.path.isdir(Setting.SAVE_DIR) : 
        os.mkdir(Setting.SAVE_DIR)

    thread = NowPlayingOnBanshee(mode)

    while True : 
        thread.run()

def GetBansheeInfo() : 
    info = {}

    info["title"] = subprocess.check_output([BANSHEE, TITLE]).decode("utf-8")
    info["artist"] = subprocess.check_output([BANSHEE, ARTIST]).decode("utf-8")
    info["album"] = subprocess.check_output([BANSHEE, ALBUM]).decode("utf-8")
    info["current"] = subprocess.check_output([BANSHEE, CURRENT_STATE]).decode("utf-8")
    info["last"] = subprocess.check_output([BANSHEE, LAST_STATE]).decode("utf-8")

    info["title"] = info["title"].replace("title: ", "").replace("\n", "")
    info["artist"] = info["artist"].replace("artist: ", "").replace("\n", "")
    info["album"] = info["album"].replace("album: ", "").replace("\n", "")
    info["current"] = info["current"].replace("current-state: ", "").replace("\n", "")
    info["last"] = info["last"].replace("last-state: ", "").replace("\n", "")

    spl_str = re.split('\(', info["title"])
    tmp_str = ''

    for x in spl_str : 
        if ')' in x : 
            x = re.sub('.*\)', '', x)

        tmp_str += x

    info["title"] = tmp_str

    spl_str = re.split('\(', info["artist"])
    tmp_str = ''

    for x in spl_str : 
        if ')' in x : 
            x = re.sub('.*\)', '', x)

        tmp_str += x

    info["artist"] = tmp_str

    return info


def showLyrics(info) : 
    filename = info['title'] + '.txt'
    lyric_pass = os.path.abspath(Setting.LYRIC_SAVE + '/' + info['artist'] + '/' + filename)

    if os.path.isfile(lyric_pass) : 
        print("This lyrics file has already been got.")

    else : 
        lyric_pass = GetLyrics.getLyrics(info['title'], info['artist'], Setting.LYRIC_SAVE +  '/' + info['artist'])

    if lyric_pass == False : 
        print("This lyrics file has not found")

    else : 
        with open(lyric_pass, 'rt') as f : 
            lyric = f.read()
            print(lyric)

def TweetInfo(info) : 
    # Tweetする文字数に注意！
    tweet = "#NowPlaying\nTitle : " + info["title"] + "\nArtist : " + info["artist"]
    
    if len(tweet) > Setting.TWEET_LIMITED : 
        # 文字数オーバー
        print('over {0} characters'.format(Setting.TWEET_LIMITED - len(tweet)))
        print('Stop tweeting')

        return False
        
    else : 
        # カバーアートの取得
        cover_pass = os.path.abspath(Setting.SAVE_DIR) + '/' + info['album']

        if os.path.isfile(cover_pass + ".jpg") : 
                cover_pass += ".jpg"
                print('This cover has already been got.')
        
        elif os.path.isfile(cover_pass + ".png") : 
                cover_pass += ".png"
                print('This cover has already been got.')

        else : 
            cover_pass = GetImages.GetImages(info["album"], info["album"], Setting.SAVE_DIR)
            print('This cover has been got successfull. pass : {0}'.format(cover_pass))
    
        # 画像の投稿
        cover_file = {"media" : open(cover_pass, 'rb')}
        req_media = Twitter.post(Setting.MEDIA_URL, files = cover_file)
    
        if req_media.status_code == 200 : 
            # 画像のアップロードに成功
            media_id = json.loads(req_media.text)['media_id']
        
            params = {
                    "status": tweet,
                    "media_ids" : [media_id] 
                    }
            req = Twitter.post("https://api.twitter.com/1.1/statuses/update.json" , params = params)
        
            if req.status_code == 200 : 
                 return True
        
            else : 
                 return False

def TestTweetInfo() : 
    info = GetBansheeInfo()
    res = TweetInfo(info)
    print(res)

def TestTweet() : 
    params = {"status": "テストツイート3"}
    req = Twitter.post("https://api.twitter.com/1.1/statuses/update.json", params = params)
    #req = Twitter.post(Setting.TWITTER_URL, params = params)

    if req.status_code == 200 : 
        print("success")

    else : 
        print("Failed to tweet")

if __name__ == "__main__" : 
    argv = sys.argv

    if len(argv) >= 2 : 
        Main(argv[1])

    else : 
        Main()

    #TestTweet()
    #TestTweetInfo()
