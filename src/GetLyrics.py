#! coding:UTF-8

'''
    歌詞検索サイトから歌詞を抽出

'''
import urllib.request
import urllib.parse
from bs4 import BeautifulSoup
import sys
import os
import Setting

def _getLyricsByAZlyrics(url) : 
    html = urllib.request.urlopen(url)
    soup = BeautifulSoup(html, "html.parser")

    or_lyric = soup.find('div', class_ = '', id = '')
    or_lyric = or_lyric.text

    lyric = or_lyric.replace('<br/>', '')

    return lyric

def getMusicListByAZlyrics(title = '', artist = '') : 
    title = urllib.parse.quote_plus(title)
    artist = urllib.parse.quote_plus(artist)

    url = Setting.AZLYRICS_URL + 'search.php?q={}'.format(artist + ' ' + title)

    html = urllib.request.urlopen(url)
    soup = BeautifulSoup(html, "html.parser")

    search_list = soup.find_all("td", class_ = "text-left visitedlyr")

    title_array = []
    artist_array = []
    url_array = []
    music_list = []
    for music in search_list : 
        tmp = music.find_all('b')
        tmp_lyrics = music.find('a')

        if len(tmp) < 2 : 
            continue

        title_array.append(tmp[0].text)
        artist_array.append(tmp[1].text)
        url_array.append(tmp_lyrics['href'])

    for x in range(len(title_array)) :
        tmp_music = {}
        tmp_music['title'] = title_array[x]
        tmp_music['artist'] = artist_array[x]
        tmp_music['url'] = url_array[x]

        music_list.append(tmp_music)

    return (music_list, len(music_list))

def getMusicListByJlyrics(title = '', artist = '') :

    title = urllib.parse.quote_plus(title)
    artist = urllib.parse.quote_plus(artist)

    url = Setting.JLYRIC_URL + 'index.php?kt={0}&ct=2&ka={1}&ca=2&kl=&cl=0'.format(title, artist)

    html = urllib.request.urlopen(url)

    soup = BeautifulSoup(html, 'html.parser')
    lylicList = soup.find_all('div', id = 'lyricList')

    title_array = []
    artist_array = []
    url_array = []
    music_list = []
    for x in lylicList :
        for music in x.find_all('div', class_ = 'title') :
            tmp = music.find('a')
            title_array.append(tmp.text)
            url_array.append(Setting.JLYRIC_URL + tmp.attrs['href'])

        for music in x.find_all('div', class_ = 'status') :
            tmp = music.find('a')
            artist_array.append(tmp.text)

    for x in range(len(title_array)) :
        tmp_music = {}
        tmp_music['title'] = title_array[x]
        tmp_music['artist'] = artist_array[x]
        tmp_music['url'] = url_array[x]

        music_list.append(tmp_music)

    return (music_list, len(music_list))


def _getLyricsByJlyrics(url) :
    html = urllib.request.urlopen(url)
    soup = BeautifulSoup(html, "html.parser")

    or_lyric = soup.find('p', id = 'lyricBody')
    or_lyric = or_lyric.text

    # 整形
    lyric = or_lyric.replace('<br/>', '')

    return lyric

def getLyrics(title = '', artist = '', filepass = 'lyrics') :
    # MusicListの取得
    music_list, musics = getMusicListByJlyrics(title, artist)
    search_flag = 0

    if musics == 0 : 
        music_list, musics = getMusicListByJlyrics(title)

        if musics == 0 :
            music_list, musics = getMusicListByAZlyrics(title, artist)
            search_flag = 1

        if musics == 0 : 
            pass

    if musics >= 2 : 
        # ユーザーレスポンス
        for x in range(musics) :
            print("No.{}".format(x + 1))
            print("Title : {}".format(music_list[x]['title']))
            print("Artist : {}\n".format(music_list[x]['artist']))
    
        print("Which would you like to get the lyrics from?")
        while True :
            num = input()
    
            if num.isdigit() == True :
                break
    
        num = int(num) - 1
    
    elif musics == 1 : 
        num = 0

    else : 
        return False

    # 曲の決定、歌詞の抽出
    if search_flag == 0 : 
        print("Get the lyrics from \'J-Lyrics\'")
        lyrics = _getLyricsByJlyrics(music_list[num]['url'])

    elif search_flag == 1 : 
        print("Get the lyrics from \'A-Z Lyrics\'")
        lyrics = _getLyricsByAZlyrics(music_list[num]['url'])

    title = music_list[num]['title']
    artist = music_list[num]['artist']
 
    # 歌詞の保存
    if os.path.isdir(filepass) == False : 
        os.mkdir(filepass)
 
    filename = title + '.txt'
 
    if os.path.isdir(filepass) == False : 
        os.mkdir(filepass)

    filepass += '/' + artist
 
    if os.path.isdir(filepass) == False : 
        os.mkdir(filepass) 

    with open(os.path.join(filepass, filename), 'wb') as f : 
        f.write(lyrics.encode('utf-8'))
 
    return os.path.abspath(filepass + '/' + filename)


if __name__ == "__main__" :
    arg = sys.argv

    if len(arg) >= 3 :
        getLyrics(arg[1], arg[2], filepass = Setting.LYRIC_SAVE)

    elif len(arg) >= 2 :
        getLyrics(arg[1], filepass = Setting.LYRIC_SAVE)
        #getMusicListByAZlyrics(arg[1])

    else :
        print("Usage : [command] [title] [artist")
