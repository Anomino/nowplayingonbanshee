
青く広がる 雨上がりの空
変わり映えしない街 虹が出ても ココじゃ気付けない
高架下の壁 派手な色の落書き
賑わう交差点で すれ違う 誰もが 孤独だ

Everything ゴミの様で 宝みたいなものさ

誰とだって 繋がる世界で
求めあって 傷をうまくふさいでる
叫んだって 届かぬ世界で
ありがとう。って 言える人を探してた
誰に会いたい?

夜中の笑い声 混み合った朝のホーム
投げ捨てられたチラシ 踏まれすぎて 塵になっていく
優しくもなれず 強くもなれなかった
だけど昨日と違う 今日をいつも普通に暮らしている

Everyday ゴミの様で 宝みたいなものさ

会いたくって 会えない人よ
僕はどうして 君に聞いてほしいんだろう?
叫んだって 届かぬ世界で
さよなら。って 言える人を探してた
誰に会いたい?

生まれ変われるなら…。 誰かがそう言っていた
それじゃあ傷は消えても 君がいない Oh Oh

宝なんて ゴミの様さ

誰とだって 繋がる世界で
求めあって 傷をうまくふさいでる
叫んだって 届かぬ世界で
ありがとう。って 言える人を探してた
君に会いたい

外は夏の雨が
激しく叩きつける
そして僕らはまた 不安を愛してしまう
やがて雲は流れ
欠けた虹が 顔を出した
追いかけては 消えても きっとある Oh Oh Oh
